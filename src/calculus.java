public class calculus {
    int first;
    int second;
    public calculus(){}
    public calculus(int first, int second){
        this.first=first;
        this.second=second;

    }

    public static void main(String args[])  //static method
    {
        int first=74;
        int second=36;
        calculus math1=new calculus(first,second);


        System.out.println(math1.addition());
        System.out.println("Static method");
        math1.finalize();

    }

    protected void finalize()
    {
        System.out.println("Object is destroyed by the Garbage Collector");
    }

    public int addition(){

        return this.first+this.second;
    }
}